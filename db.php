<?php
include_once("utils/include.php");
  $mysqli_link = mysqli_connect($config["mysqli_ip"],$config["mysqli_user"],$config["mysqli_pass"],$config["mysqli_db"]);

  if (!$mysqli_link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
  }
?>
