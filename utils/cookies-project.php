<?php
  function SetLogin($pid)
  {
    setcookie("pm_pidlogin", true);
    setcookie("pm_pidloginid", $pid);
  }

  function GetLogin(){
    $pid = $_COOKIE["pm_pidloginid"];

    if($_COOKIE["pm_pidlogin"] && strlen($pid) == 32){
      return true;
    }else{
      return false;
    }
  }

  function SetLogout(){
    setcookie("pm_pidlogin", false);
    setcookie("pm_pidloginid", 0);
  }

  function RedirectProjectIndex(){
      while (ob_get_status())
      {
          ob_end_clean();
      }
      header( "Location: ?page=project-home" );
  }
 ?>
