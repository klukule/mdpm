<?php
  $lang["project-login-enter-pid"] = "Enter project ID";
  $lang["project-login-enter-pid-input"] = "Project ID";

  $lang["project-login-error-pid"] = "Project doesn't exists";
  $lang["project-login-error-db"] = "Error when reading from database";
  $lang["project-login-error-not-authorized"] = "You must login first";

  $lang["project-login-info-logout"] = "Successfully logged out";


  $lang["name-html"] = "<b>Project</b>Manager";
  $lang["name-text"] = "Project Manager";
  $lang["name-small-html"] = "<b>P</b>M";
  $lang["all-rights-reserved"] = "All rights reserved";
  $lang["404"] = "Error 404 - page not found";
 ?>
