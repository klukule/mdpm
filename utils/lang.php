<?php
  function Lang($value='')
  {
    global $config;
    global $lang;

    if($config["debug"] == true){
      echo $value;
    }else{
      if (array_key_exists($value, $lang)) {
        echo $lang[$value];
      }else{
        echo $value;
      }
    }
  }
?>
