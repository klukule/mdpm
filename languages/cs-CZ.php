<?php
  $lang["project-login-enter-pid"] = "Zadejte ID projektu";
  $lang["project-login-enter-pid-input"] = "ID projektu";

  $lang["project-login-error-pid"] = "Projekt neexistuje";
  $lang["project-login-error-db"] = "Chyba při čtení z databáze";
  $lang["project-login-error-not-authorized"] = "Nejprve se musíte přihlásit";

  $lang["project-login-info-logout"] = "Byl jste úspěšně odhlášen";


  $lang["name-html"] = "<b>Project</b>Manager";
  $lang["name-text"] = "Project Manager";
  $lang["name-small-html"] = "<b>P</b>M";
  $lang["all-rights-reserved"] = "Všechna práva vyhrazena";
  $lang["404"] = "Chyba 404 - stránka nenalezena";
 ?>
