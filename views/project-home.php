<?php
include("utils/cookies-project.php");
if(!GetLogin()){
  while (ob_get_status())
  {
      ob_end_clean();
  }
  header( "Location: ?page=project-login&error=3" );
}
if(isset($_GET['action']))
{
   switch ($_GET['action']) {
     case 'logout':
       SetLogout();
       while (ob_get_status())
       {
           ob_end_clean();
       }
       header( "Location: ?page=project-login&info=1" );
       break;

     default:
       break;
   }
}

 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title><? Lang("name-text");?> | Project home</title>
     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
     <link rel="stylesheet" href="assets/dist/css/Admin.min.css">

     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->

     <!--
     |---------------------------------------------------------|
     | SKINY         | skin-blue                               |
     |               | skin-black                              |
     |               | skin-purple                             |
     |               | skin-yellow                             |
     |               | skin-red                                |
     |               | skin-green                              |
     |---------------------------------------------------------|
     |LAYOUT         | fixed                                   |
     |               | layout-boxed                            |
     |               | layout-top-nav                          |
     |               | sidebar-collapse                        |
     |               | sidebar-mini                            |
     |---------------------------------------------------------|
    -->
     <link rel="stylesheet" href="assets/dist/css/skins/skin-blue.min.css">
   </head>

   <body class="hold-transition skin-blue sidebar-mini">
     <div class="wrapper">

       <header class="main-header">
         <a href="?page=project-home" class="logo">
           <span class="logo-mini"><? Lang("name-small-html");?></span>
           <span class="logo-lg"><? Lang("name-html");?></span>
         </a>
         <nav class="navbar navbar-static-top" role="navigation">
           <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
             <span class="sr-only">Toggle navigation</span>
           </a>
           <div class="navbar-custom-menu">
             <ul class="nav navbar-nav">
               <li class="dropdown user user-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   Dropdown</a>
                 <ul class="dropdown-menu">
                   <li class="user-footer">
                     <div class="pull-right">
                       <a href="?page=project-home&action=logout" class="btn btn-default btn-flat">Sign out</a>
                     </div>
                   </li>
                 </ul>
               </li>
             </ul>
           </div>
         </nav>
       </header>
       <aside class="main-sidebar">
         <section class="sidebar">

           <!-- Sidebar Menu -->
           <ul class="sidebar-menu">
             <li class="header">HEADER</li>
             <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
             <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
             <li class="treeview">
               <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                 <li><a href="#">Link in level 2</a></li>
                 <li><a href="#">Link in level 2</a></li>
               </ul>
             </li>
           </ul>
         </section>
       </aside>

       <div class="content-wrapper">
         <section class="content-header">
           <h1>
             Page Header
             <small>Optional description</small>
           </h1>
           <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
             <li class="active">Here</li>
           </ol>
         </section>

         <section class="content">
            TADY BUDE OBSAH

         </section>
       </div>

       <footer class="main-footer">
         <div class="pull-right hidden-xs">
           Anything you want
         </div>
         <strong>Copyright &copy; 2016 <a href="http://moowdesign.eu" class="text-black">Moowdesign.eu</a>.</strong> <? Lang("all-rights-reserved"); ?>.
       </footer>
     </div>


     <script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
     <script src="assets/bootstrap/js/bootstrap.min.js"></script>
     <script src="assets/dist/js/app.min.js"></script>
   </body>
 </html>
