<?
include("utils/cookies-project.php");
if(GetLogin()){
  RedirectProjectIndex();
}

function CheckInDB()
{
  global $mysqli_link;
  $var = $_POST["project-id"];
  if($result = $mysqli_link->query("SELECT * FROM pm_project WHERE hash = '".$var."'")){
    if($result->num_rows > 0 && strlen($var) == 32){
      SetLogin($var);
      RedirectProjectIndex();
    }else{
      while (ob_get_status())
      {
          ob_end_clean();
      }
      header( "Location: ?page=project-login&error=1" );
    }
    $result->close();
  }else{
    while (ob_get_status())
    {
        ob_end_clean();
    }
    header( "Location: ?page=project-login&error=2" );
  }

}

if(isset($_POST['submit']))
{
   CheckInDB();
}

?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><? Lang("name-text"); ?> | Log in</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/dist/css/Admin.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="?page=index"><? Lang("name-html"); ?></a>
      </div>
      <div class="lockscreen-name"><?
      if($_GET["error"] == 1){

        echo "<div class='text-red'>";
        Lang("project-login-error-pid");
        echo"</div>";

      }else if($_GET["error"] == 2){

        echo "<div class='text-red'>";
        Lang("project-login-error-db");
        echo"</div>";

      }else if($_GET["error"] == 3){

        echo "<div class='text-red'>";
        Lang("project-login-error-not-authorized");
        echo"</div>";

      }else if($_GET["info"] == 1){

        echo "<div class='text-green'>";
        Lang("project-login-info-logout");
        echo"</div>";

      }else{
        Lang("project-login-enter-pid");
      }


       ?></div>

      <div class="lockscreen-item">
        <div class="lockscreen-image">
          <img src="assets/dist/img/temp-128x128.jpg" alt="User Image">
        </div>

        <form class="lockscreen-credentials" action="?page=project-login" method="post">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="<? Lang("project-login-enter-pid-input"); ?>" name="project-id">
            <div class="input-group-btn">
              <button class="btn" name="submit"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form>

      </div>

      <div class="lockscreen-footer text-center">
        Copyright &copy; 2016 <b><a href="http://moowdesign.eu" class="text-black">Moowdesign.eu</a></b><br>
        <? Lang("all-rights-reserved"); ?>
      </div>
    </div>
    <script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
